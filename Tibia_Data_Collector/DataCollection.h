#pragma once
class DataCollection
{
	/********************************************************
		RESEARCH OR OTHER IMPORTANT INFO TO LOOK INTO:
		-	Look for library for file exports to spss or
			other data analysis programs.

		-
	*********************************************************/
	
	/********************************************************
		REMINDERS:
		-	Don't create class wide variables if possible.

		-	
	*********************************************************/

	/********************************************************
		TODO:
		-	Create a constructors for data import to class
			based on different data needed to be collected.
			I.E. combat, spell, purchase, trade etc...
		
		-	Create function to export data to file.

		-	
	*********************************************************/
public:
	DataCollection();
	~DataCollection();
private:

};

